import React, { Component } from 'react';
import {Helmet} from 'react-helmet'
import '../../common/index.scss';
import Slider from 'react-slick';
import {Link} from 'react-router-dom'
// Import custom components
import HeaderFour from "../../common/headers/header-four"
import SpecialProducts from "./special-products"
import FooterOne from "../../common/footers/footer-one";
import ThemeSettings from "../../common/theme-settings";
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";

class Electronic extends Component {

    componentDidMount() {
        document.getElementById("color").setAttribute("href", `${process.env.PUBLIC_URL}/assets/css/color7.css` );
    }

    render(){
        const responsive = {
            desktop: {
              breakpoint: { max: 3000, min: 1024 },
              items: 3,
              slidesToSlide: 3, // optional, default to 1.
            },
            tablet: {
              breakpoint: { max: 1024, min: 464 },
              items: 2,
              slidesToSlide: 2, // optional, default to 1.
            },
            mobile: {
              breakpoint: { max: 464, min: 0 },
              items: 1,
              slidesToSlide: 1, // optional, default to 1.
            },
          };

        return (
            <div className="container-fluid layout-8">
                <Helmet>
                    <title>Buy/Sell Cryptocurrencies | GXBrokerStore</title>
                </Helmet>
<a href={`${process.env.PUBLIC_URL}/`}>
                <HeaderFour logoName={'logo/3.png'} />
            
            </a>
                <section className="p-0 padding-bottom-cls">
                    <Slider className="slide-1 home-slider">
                        <div>
                            <div className="home home15">
                                <div className="container">
                                    <div className="row">
                                        <div className="col">
                                            <div className="slider-contain">
                                                <div>
                                                    <h4>Introducing The World's First</h4>
                                                    <h1 style={{textTransform:"none"}}>ExchangeOS</h1>
                                                    <a href="#" className="btn btn-outline btn-classic">Watch Video</a></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div className="home home16">
                                <div className="container">
                                    <div className="row">
                                        <div className="col">
                                            <div className="slider-contain">
                                                <div>
                                                    <h4>Introducing The World's First</h4>
                                                    <h1>ExchangeOS</h1>
                                                    <a href="#" className="btn btn-outline btn-classic">Watch Video</a></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Slider>
                </section>

                <div className="layout-8-bg">
                    {/*About Section*/}
                    {/* <section className="banner-goggles ratio2_3">
                        <div className="container-fluid">
                            <div className="row partition3">
                                <div className="col-md-4">
                                    <a href="#">
                                        <div className="collection-banner">
                                            <div className="img-part">
                                                <img src={`${process.env.PUBLIC_URL}/assets/images/electronics/sub1.png`}
                                                     className="img-fluid blur-up lazyload bg-img" alt="" />
                                            </div>
                                            <div className="contain-banner banner-3">
                                                <div>
                                                    <h4>Buy</h4>
                                                    <h3 style={{color:"black"}}>Bitcoin</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div className="col-md-4">
                                    <a href="#">
                                        <div className="collection-banner">
                                            <div className="img-part">
                                                <img src={`${process.env.PUBLIC_URL}/assets/images/electronics/sub2.png`}
                                                     className="img-fluid blur-up lazyload bg-img" alt="" />
                                            </div>
                                            <div className="contain-banner banner-3">
                                                <div>
                                                    <h4>Buy</h4>
                                                    <h3 style={{color:"black"}}>Ethereum</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div className="col-md-4">
                                    <a href="#">
                                        <div className="collection-banner">
                                            <div className="img-part">
                                                <img src={`${process.env.PUBLIC_URL}/assets/images/electronics/sub3.png`}
                                                     className="img-fluid blur-up lazyload bg-img" alt="" />
                                            </div>
                                            <div className="contain-banner banner-3">
                                                <div>
                                                    <h4>Buy</h4>
                                                    <h3 style={{color:"black"}}>Litecoin</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </section> */}
                    <Carousel
                    
  swipeable={false}
  draggable={false}
//   showDots={true}
  responsive={responsive}
  ssr={true} // means to render carousel on server-side.
  infinite={true}
//   autoPlay={this.props.deviceType !== "mobile" ? true : false}
//   autoPlaySpeed={1000}
  keyBoardControl={true}
  customTransition="all .5"
  transitionDuration={500}
  containerClass="carousel-container"
  removeArrowOnDeviceType={["tablet", "mobile"]}
  deviceType={this.props.deviceType}
  dotListClass="custom-dot-list-style"
  itemClass="carousel-item-padding-40-px"
>
<div className="collection-banner">
                                            <div className="img-part">
                                                <img src={`${process.env.PUBLIC_URL}/assets/images/electronics/sub1.png`}
                                                     className="img-fluid blur-up lazyload bg-img" alt="" />
                                            </div>
                                            <div className="contain-banner banner-3">
                                                <div>
                                                    <h4 style={{color:"#117abf"}}>Buy</h4>
                                                    <h3 style={{color:"black"}}>Bitcoin</h3>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="collection-banner">
                                            <div className="img-part">
                                                <img src={`${process.env.PUBLIC_URL}/assets/images/electronics/sub2.png`}
                                                     className="img-fluid blur-up lazyload bg-img" alt="" />
                                            </div>
                                            <div className="contain-banner banner-3">
                                                <div>
                                                    <h4 style={{color:"#117abf"}}>Buy</h4>
                                                    <h3 style={{color:"black"}}>Ethereum</h3>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="collection-banner">
                                            <div className="img-part">
                                                <img src={`${process.env.PUBLIC_URL}/assets/images/electronics/sub3.png`}
                                                     className="img-fluid blur-up lazyload bg-img" alt="" />
                                            </div>
                                            <div className="contain-banner banner-3">
                                                <div>
                                                    <h4 style={{color:"#117abf"}}>Buy</h4>
                                                    <h3 style={{color:"black"}}>Litecoin</h3>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        
  
</Carousel>
                    {/*About Section End*/}

                    {/*Product slider*/}
                    <SpecialProducts type={'electronics'} />
                    {/*Product slider End*/}
                </div>
                <div className="footer-white">
                    <FooterOne logoName={'logo/3.png'} />
                </div>

                <ThemeSettings />
            </div>
        )
    }
}


export default Electronic;
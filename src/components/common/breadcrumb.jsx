import React, {Component} from 'react';
import {Link} from 'react-router-dom'
import '../NewComponents/css/test.css'
import bitcoin from '../NewComponents/Images/bitcoin.png'
class Breadcrumb extends Component {
    render (){
        const {title, parent} = this.props;
        return (
            <div className="breadcrumb-section" style={{backgroundColor:"#117abf"}}>
                <div className="container">
                    <div className="row">
                        <div className="col-md-6">
                            <div className="page-title">
                                {/* <h2>{Bit}</h2> */}
                                {/* <h2>Cryptocurrencies</h2> */}
                                <img src={bitcoin} style={{width:"30px"}} alt=""/>
                                <h2 style={{padding:"5px",color:"white"}}>{title}</h2>
                            </div>
                        </div>
                        <div className="col-md-6">
                            <nav aria-label="breadcrumb" className="theme-breadcrumb">
                                <ol className="breadcrumb">
                                    <li className="breadcrumb-item " style={{color:"white"}}>GXBroker/ Cryptocurrencies/{parent}</li>
                                    {/* <li className="breadcrumb-item " ><Link to={`${process.env.PUBLIC_URL}`} style={{color:"white"}}>GXBroker/ Cryptocurrencies/{parent}</Link></li> */}
                                    {/* {parent?
                                    <li className="breadcrumb-item" aria-current="page">{parent}</li>:''}
                                    <li className="breadcrumb-item active" aria-current="page">{title}</li> */}
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Breadcrumb;
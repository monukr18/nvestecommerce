import React, {Component} from 'react';
import Slider from 'react-slick';
import '../common/index.scss';
import {connect} from "react-redux";
//CSS
import '../NewComponents/css/test.css'
// Images
import marketcap from '../NewComponents/Images/marketcap.svg';
import volume from '../NewComponents/Images/24volume.svg';
import trust from '../NewComponents/Images/trust.svg';
import circulation from '../NewComponents/Images/circulating.svg';
import supply from '../NewComponents/Images/supply.svg';
import inflation from '../NewComponents/Images/inflation.svg';
import ratio from '../NewComponents//Images/ratio.svg';
import firstannounced from '../NewComponents/Images/loudspeaker.svg';
import blockchain from '../NewComponents/Images/blockchain.svg';
import work from '../NewComponents/Images/consenus.svg'
import timeclock from '../NewComponents/Images/clock.svg'
// import algo from '../NewComponents/Images/'


// import custom Components
// import RelatedProduct from "../common/related-product"
import Breadcrumb from "../common/breadcrumb";
import Details from "./common/product/details";
import Price from "./common/product/price";
import DetailsTopTabs from "./common/details-top-tabs";
import {addToCart, addToCartUnsafe, addToWishlist } from '../../actions'
import ImageZoom from './common/product/image-zoom'
import SmallImages from './common/product/small-image';

import Graph from '../NewComponents/Graph/graph'




class Column extends Component {

    constructor() {
        super();
        this.state = {
            nav1: null,
            nav2: null,
            currdetails:
            
            [{name:'MARKET CAP',crdetail:'$1,150222',icon:marketcap },
            {name:'REPORTED VOLUME 24H',crdetail:'$1,150222',icon:volume },
            {name:'TRUSTED VOLUME 24H',crdetail:'$1,150222',icon:trust },
            {name:'CIRCULATING SUPPLY',crdetail:'1,150222',icon:circulation },
            {name:'TOTAL SUPPLY',crdetail:'1,150222',icon:supply },
            {name:'INFLATION',crdetail:'Max Supply (21M BTC',icon:inflation } ,  
            {name:'NVT RATIO',crdetail:'1,150222',icon:ratio } ,  
            {name:'FIRST ANNOUNCED',crdetail:'January 3, 2009',icon:firstannounced } ,  
            ],
            currtechnicaldetails:
            [{name:'BLOCKCHAIN',imagee:blockchain ,type:'Own Blockchain'}
            ,{name:'CONSENUS',imagee:work ,type:'Proof of Work (PoW)'}
            ,{name:'BLOCK TIME',imagee:timeclock ,type:'10 minutes'}
            ,{name:'ALGORITHM',imagee:timeclock ,type:'SHA256'}
            ],
            currwallet:
            [{name:'Jaxx',imagee:blockchain ,type:'Multi-cryptocurrency wallet'}
            ,{name:'Bitpay',imagee:work ,type:'Secure bitcoin and bitcoin cash wallet'}
            ,{name:'BTC.com Wallet App',imagee:timeclock ,type:'wallet for bitcoin and bitcoin cash'}
            ,{name:'Enjin wallet',imagee:timeclock ,type:'Ethereum Bitcoin ERC20 & LTC Wallet'}
            ,{name:'Bitpay',imagee:work ,type:'Secure bitcoin and bitcoin cash wallet'}
            ,{name:'BTC.com Wallet App',imagee:timeclock ,type:'wallet for bitcoin and bitcoin cash'}
            ,{name:'Enjin wallet',imagee:timeclock ,type:'Ethereum Bitcoin ERC20 & LTC Wallet'}
            ,{name:'Bitpay',imagee:work ,type:'Secure bitcoin and bitcoin cash wallet'}
            ,{name:'BTC.com Wallet App',imagee:timeclock ,type:'wallet for bitcoin and bitcoin cash'}
            ,{name:'Enjin wallet',imagee:timeclock ,type:'Ethereum Bitcoin ERC20 & LTC Wallet'}
            ,{name:'Bitpay',imagee:work ,type:'Secure bitcoin and bitcoin cash wallet'}
            ,{name:'BTC.com Wallet App',imagee:timeclock ,type:'wallet for bitcoin and bitcoin cash'}
            ,{name:'Enjin wallet',imagee:timeclock ,type:'Ethereum Bitcoin ERC20 & LTC Wallet'}
            ]
        };
    }

    componentDidMount() {
        this.setState({
            nav1: this.slider1,
            nav2: this.slider2
        });

    }

    render(){
        const {symbol, item, addToCart, addToCartUnsafe, addToWishlist} = this.props
        var products = {
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: false,
            arrows: true,
            fade: true
        };
        var productsnav = {
            slidesToShow: 3,
            swipeToSlide:true,
            arrows: false,
            dots: false,
            focusOnSelect: true
        };

        return (
            <div>

                <Breadcrumb  title={' Product / '+item.name} />

                {/*Section Start*/}
                {(item)?
                    <section >
                        <div className="collection-wrapper">
                            <div className="container">
                                <div className="row">
                                    <div className="col-lg-8 product-thumbnail">
                                        {/* <Slider {...products} asNavFor={this.state.nav2} ref={slider => (this.slider1 = slider)} className="product-right-slick">
                                            {item.variants.map((vari, index) =>
                                                <div key={index}>
                                                    <ImageZoom image={vari.images} className="img-fluid image_zoom_cls-0" />
                                                </div>
                                            )}
                                        </Slider>
                                        <SmallImages item={item} settings={productsnav} navOne={this.state.nav1} /> */}

                                     
                                      <Graph />


                                    </div>
                                    {/* Product Details */}
                                    {/* <Details item={item} addToWishlistClicked={addToWishlist} /> */}
                                    {/* Product Price Details */}
                                    <Price symbol={symbol}  item={item} navOne={this.state.nav1} addToCartClicked={addToCart} BuynowClicked={addToCartUnsafe} />
                                </div>
                            </div>
                        </div>
                    </section> : ''}
                {/*Section End*/}
                    {/* BitCoin Details */}
                <section className="tab-product m-0">
                    <div className="container" style={{boxShadow : "0 1px 2px 0 rgba(0,0,0,.2)"}}>
                        <div className="row">
                            <div className="col-sm-12 col-lg-12 curHeader" style={{backgroundColor:"#708090"}}>
                              <h6 style={{color:"white"}}>Bitcoin Details</h6>
                            </div>
                        </div>
                        <div className="row" >
                        {this.state.currdetails.map(detail => {
                            return(
                                <>
                               
                            <div className="col-lg-4" style={{padding:"15px"}}>
                            <img style={{width:"25px"}} src={detail.icon} alt=""/>
                                <span style={{paddingLeft:"10px"}}>{detail.name}</span>
                                <br />
                                <span style={{paddingLeft:"35px"}}>{detail.crdetail}</span>
                            </div> 
                                </>
                            )
                        })}
                        </div>
                        
                    </div>
                </section>
                      

                {/* Bitcoin Technical Details */}




                <section className="tab-product m-0">
                    <div className="container" style={{boxShadow : "0 1px 2px 0 rgba(0,0,0,.2)"}}>
                        <div className="row">
                            <div className="col-sm-12 col-lg-12 curHeader" style={{backgroundColor:"#708090"}}>
                              <h6 style={{color:"white"}}>Bitcoin Technical Details</h6>
                            </div>
                        </div>
                        <div className="row" >
                        {this.state.currtechnicaldetails.map(detail => {
                            return(
                                <>
                               
                            <div className="col-lg-4" style={{padding:"15px"}}>
                            <img style={{width:"25px"}} src={detail.imagee} alt=""/>
                                <span style={{paddingLeft:"10px"}}>{detail.name}</span>
                                <br />
                                <span style={{paddingLeft:"35px"}}>{detail.type}</span>
                            </div>
                        
                                </>
                            )
                        })}
                        </div>
                        
                    </div>
                </section>


                {/*  */}



                {/*  */}

                  {/* Bitcoin wallets */}


                <section className="tab-product m-0 pb-4">
                    <div className="container" style={{boxShadow : "0 1px 2px 0 rgba(0,0,0,.2)"}}>
                        <div className="row">
                            <div className="col-sm-12 col-lg-12 curHeader" style={{backgroundColor:"#708090"}}>
                              <h6 style={{color:"white"}}>Bitcoin Wallets</h6>
                            </div>
                        </div>
                        <div className="row" >
                        {this.state.currwallet.map(detail => {
                            return(
                                <>
                               
                            <div className="col-lg-6" style={{padding:"15px"}}>
                           <div className="walletset" style={{border: "2px #e6ecf1 solid",padding:"15px"}}>
                           <img style={{width:"25px"}} src={detail.imagee} alt=""/>
                                <span style={{paddingLeft:"10px"}}>{detail.name}</span>
                                <br />
                                <span style={{paddingLeft:"35px"}}>{detail.type}</span>
                           </div>
                            </div>
                        
                                </>
                            )
                        })}
                        </div>
                        
                    </div>
                </section>

                {/*  */}
                
                {/* <RelatedProduct /> */}
            </div>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    let productId = ownProps.match.params.id;
    return {
        item: state.data.products.find(el => el.id == productId),
        symbol: state.data.symbol
    }
}

export default connect(mapStateToProps, {addToCart, addToCartUnsafe, addToWishlist}) (Column);
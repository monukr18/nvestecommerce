import React, {Component} from 'react';
import {Helmet} from 'react-helmet'
import Breadcrumb from "../common/breadcrumb";
import NewProduct from "../common/newproduct1";
import Filter from "./common/filter1";
import FilterBar from "./common/filterbar1";
import ProductListing from "./common/productlisting1";
import StickyBox from "react-sticky-box";

class BuyInfluence extends Component {
    state = {
        layoutColumns:3
    }
    LayoutViewClicked(colums) {
        this.setState({
            layoutColumns:colums
        })
    }
    openFilter = () => {
        document.querySelector(".collection-filter").style = "left: -15px";
    }
    render (){
        return (
            <div>
                {/*SEO Support*/}
                <Helmet>
                <title>Buy/Sell Cryptocurrencies | GXBrokerStore</title>
                    <meta name="description" content="GXBroker - The World's Largest Broker Network" />
                </Helmet>
                {/*SEO Support End */}
                {/* <Breadcrumb title={'Collection'}/> */}
                <section className="section-b-space">
                    <div className="collection-wrapper">
                        <div className="container-fluid">
                            <div className="row">
                                <div className="col-sm-3 collection-filter" style={{overflowY:"auto"}}>
                                    {/* <StickyBox offsetTop={20} offsetBottom={20} > */}
                                    <StickyBox>
                                        <div>
                                            <Filter/>
                                            <NewProduct/>
                                            {/* <div className="collection-sidebar-banner">
                                                <a href="#">
                                                    <img src={`${process.env.PUBLIC_URL}/assets/images/side-banner.png`} className="img-fluid" alt="" />
                                                </a>
                                            </div> */}
                                        </div>
                                    </StickyBox>
                                    {/*side-bar banner end here*/}
                                </div>
                                <div className="collection-content col">
                                    <div className="page-main-content ">
                                        <div className="">
                                            <div className="row">
                                                <div className="col-sm-12">
                                                    <div className="top-banner-wrapper">
                                                        <a href="#"><img style={{border:"1px solid rgb(221, 221, 221)"}} src={`${process.env.PUBLIC_URL}/assets/images/mega-menu/2.jpg`} className="img-fluid" alt=""/></a>
                                                        <div className="top-banner-content small-section">
                                                            <h4>What Is A Cryptocurrency?</h4>
                                                            {/* <h5>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</h5> */}
                                                            {/* <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. </p> */}
                                                        <p>A cryptocurrency is a digital currency in which encryption techniques are used to regulate the generation of units of currency and verify the transfer of funds, operating independently of a central bank.</p>
                                                        </div>
                                                    </div>
                                                    <div className="collection-product-wrapper">
                                                        <div className="product-top-filter">
                                                            <div className="container-fluid p-0">
                                                                <div className="row">
                                                                    <div className="col-xl-12">
                                                                        <div className="filter-main-btn">
                                                                            <span onClick={this.openFilter}
                                                                                className="filter-btn btn btn-theme"><i
                                                                                className="fa fa-filter"
                                                                                aria-hidden="true"></i> Filter</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div className="row">
                                                                    <div className="col-12">
                                                                        <FilterBar onLayoutViewClicked={(colmuns) => this.LayoutViewClicked(colmuns)}/>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        {/*Products Listing Component*/}
                                                        <ProductListing colSize={this.state.layoutColumns}/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        )
    }
}
export default BuyInfluence;
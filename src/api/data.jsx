const datas =
    [{
        "id": 71,
        "selector":"selector",
        "name": "Flare Dress",
        "price": 120,
        "salePrice": 200,
        "discount": 50,
        "pictures": ["/assets/images/fashion/product/1.jpg", "/assets/images/fashion/product/21.jpg", "/assets/images/fashion/product/36.jpg", "/assets/images/fashion/product/12.jpg"],
        "shortDetails": "Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. Nemo enim ipsam voluptatem,",
        "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        "stock": 16,
        "new": true,
        "sale": true,
        "category": "women",
        "colors": ["yellow", "gray", "green"],
        "size": ["M", "L", "XL"],
        "tagsname": ["Business Service"],
        "rating": 4,
        "variants": [
        {
          "color": "yellow",
          "images": "/assets/images/fashion/product/1.jpg"
        },
        {
          "color": "gray",
          "images": "/assets/images/fashion/product/21.jpg"
        },
        {
          "color": "green",
          "images": "/assets/images/fashion/product/36.jpg"
        }]
      },
      {
        "id": 72,
        "selector":"selector",
        "name": "Flare Dress",
        "price": 120,
        "salePrice": 200,
        "discount": 50,
        "pictures": ["/assets/images/fashion/product/1.jpg", "/assets/images/fashion/product/21.jpg", "/assets/images/fashion/product/36.jpg", "/assets/images/fashion/product/12.jpg"],
        "shortDetails": "Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. Nemo enim ipsam voluptatem,",
        "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        "stock": 16,
        "new": true,
        "sale": true,
        "category": "women",
        "colors": ["yellow", "gray", "green"],
        "size": ["M", "L", "XL"],
        "tagsname": ["Crowdfunding"],
        "rating": 4,
        "variants": [
        {
          "color": "yellow",
          "images": "/assets/images/fashion/product/1.jpg"
        },
        {
          "color": "gray",
          "images": "/assets/images/fashion/product/21.jpg"
        },
        {
          "color": "green",
          "images": "/assets/images/fashion/product/36.jpg"
        }]
      },
      {
        "id": 73,
        "selector":"selector",
        "name": "Flare Dress",
        "price": 120,
        "salePrice": 200,
        "discount": 50,
        "pictures": ["/assets/images/fashion/product/1.jpg", "/assets/images/fashion/product/21.jpg", "/assets/images/fashion/product/36.jpg", "/assets/images/fashion/product/12.jpg"],
        "shortDetails": "Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. Nemo enim ipsam voluptatem,",
        "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        "stock": 16,
        "new": true,
        "sale": true,
        "category": "women",
        "colors": ["yellow", "gray", "green"],
        "size": ["M", "L", "XL"],
        "tagsname": ["Entertainment"],
        "rating": 4,
        "variants": [
        {
          "color": "yellow",
          "images": "/assets/images/fashion/product/1.jpg"
        },
        {
          "color": "gray",
          "images": "/assets/images/fashion/product/21.jpg"
        },
        {
          "color": "green",
          "images": "/assets/images/fashion/product/36.jpg"
        }]
      },
      {
        "id": 74,
        "selector":"selector",
        "name": "Flare Dress",
        "price": 120,
        "salePrice": 200,
        "discount": 50,
        "pictures": ["/assets/images/fashion/product/1.jpg", "/assets/images/fashion/product/21.jpg", "/assets/images/fashion/product/36.jpg", "/assets/images/fashion/product/12.jpg"],
        "shortDetails": "Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. Nemo enim ipsam voluptatem,",
        "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        "stock": 16,
        "new": true,
        "sale": true,
        "category": "women",
        "colors": ["yellow", "gray", "green"],
        "size": ["M", "L", "XL"],
        "tagsname": ["Healthcare"],
        "rating": 4,
        "variants": [
        {
          "color": "yellow",
          "images": "/assets/images/fashion/product/1.jpg"
        },
        {
          "color": "gray",
          "images": "/assets/images/fashion/product/21.jpg"
        },
        {
          "color": "green",
          "images": "/assets/images/fashion/product/36.jpg"
        }]
      },
      {
        "id": 75,
        "selector":"selector",
        "name": "Flare Dress",
        "price": 120,
        "salePrice": 200,
        "discount": 50,
        "pictures": ["/assets/images/fashion/product/1.jpg", "/assets/images/fashion/product/21.jpg", "/assets/images/fashion/product/36.jpg", "/assets/images/fashion/product/12.jpg"],
        "shortDetails": "Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. Nemo enim ipsam voluptatem,",
        "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        "stock": 16,
        "new": true,
        "sale": true,
        "category": "women",
        "colors": ["yellow", "gray", "green"],
        "size": ["M", "L", "XL"],
        "tagsname": ["Manufacturing"],
        "rating": 4,
        "variants": [
        {
          "color": "yellow",
          "images": "/assets/images/fashion/product/1.jpg"
        },
        {
          "color": "gray",
          "images": "/assets/images/fashion/product/21.jpg"
        },
        {
          "color": "green",
          "images": "/assets/images/fashion/product/36.jpg"
        }]
      },
      {
        "id": 76,
        "selector":"selector",
        "name": "Flare Dress",
        "price": 120,
        "salePrice": 200,
        "discount": 50,
        "pictures": ["/assets/images/fashion/product/1.jpg", "/assets/images/fashion/product/21.jpg", "/assets/images/fashion/product/36.jpg", "/assets/images/fashion/product/12.jpg"],
        "shortDetails": "Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. Nemo enim ipsam voluptatem,",
        "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        "stock": 16,
        "new": true,
        "sale": true,
        "category": "women",
        "colors": ["yellow", "gray", "green"],
        "size": ["M", "L", "XL"],
        "tagsname": ["Prediction"],
        "rating": 4,
        "variants": [
        {
          "color": "yellow",
          "images": "/assets/images/fashion/product/1.jpg"
        },
        {
          "color": "gray",
          "images": "/assets/images/fashion/product/21.jpg"
        },
        {
          "color": "green",
          "images": "/assets/images/fashion/product/36.jpg"
        }]
      },
      {
        "id": 77,
        "selector":"selector",
        "name": "Flare Dress",
        "price": 120,
        "salePrice": 200,
        "discount": 50,
        "pictures": ["/assets/images/fashion/product/1.jpg", "/assets/images/fashion/product/21.jpg", "/assets/images/fashion/product/36.jpg", "/assets/images/fashion/product/12.jpg"],
        "shortDetails": "Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. Nemo enim ipsam voluptatem,",
        "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        "stock": 16,
        "new": true,
        "sale": true,
        "category": "women",
        "colors": ["yellow", "gray", "green"],
        "size": ["M", "L", "XL"],
        "tagsname": ["Ticketing"],
        "rating": 4,
        "variants": [
        {
          "color": "yellow",
          "images": "/assets/images/fashion/product/1.jpg"
        },
        {
          "color": "gray",
          "images": "/assets/images/fashion/product/21.jpg"
        },
        {
          "color": "green",
          "images": "/assets/images/fashion/product/36.jpg"
        }]
      },
      {
        "id": 78,
        "selector":"selector",
        "name": "Flare Dress",
        "price": 120,
        "salePrice": 200,
        "discount": 50,
        "pictures": ["/assets/images/fashion/product/1.jpg", "/assets/images/fashion/product/21.jpg", "/assets/images/fashion/product/36.jpg", "/assets/images/fashion/product/12.jpg"],
        "shortDetails": "Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. Nemo enim ipsam voluptatem,",
        "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        "stock": 16,
        "new": true,
        "sale": true,
        "category": "women",
        "colors": ["yellow", "gray", "green"],
        "size": ["M", "L", "XL"],
        "tagsname": ["Advertising"],
        "rating": 4,
        "variants": [
        {
          "color": "yellow",
          "images": "/assets/images/fashion/product/1.jpg"
        },
        {
          "color": "gray",
          "images": "/assets/images/fashion/product/21.jpg"
        },
        {
          "color": "green",
          "images": "/assets/images/fashion/product/36.jpg"
        }]
      },
      {
        "id": 79,
        "selector":"selector",
        "name": "Flare Dress",
        "price": 120,
        "salePrice": 200,
        "discount": 50,
        "pictures": ["/assets/images/fashion/product/1.jpg", "/assets/images/fashion/product/21.jpg", "/assets/images/fashion/product/36.jpg", "/assets/images/fashion/product/12.jpg"],
        "shortDetails": "Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. Nemo enim ipsam voluptatem,",
        "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        "stock": 16,
        "new": true,
        "sale": true,
        "category": "women",
        "colors": ["yellow", "gray", "green"],
        "size": ["M", "L", "XL"],
        "tagsname": ["Cannabis"],
        "rating": 4,
        "variants": [
        {
          "color": "yellow",
          "images": "/assets/images/fashion/product/1.jpg"
        },
        {
          "color": "gray",
          "images": "/assets/images/fashion/product/21.jpg"
        },
        {
          "color": "green",
          "images": "/assets/images/fashion/product/36.jpg"
        }]
      },
      {
        "id": 80,
        "selector":"selector",
        "name": "Flare Dress",
        "price": 120,
        "salePrice": 200,
        "discount": 50,
        "pictures": ["/assets/images/fashion/product/1.jpg", "/assets/images/fashion/product/21.jpg", "/assets/images/fashion/product/36.jpg", "/assets/images/fashion/product/12.jpg"],
        "shortDetails": "Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. Nemo enim ipsam voluptatem,",
        "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        "stock": 16,
        "new": true,
        "sale": true,
        "category": "women",
        "colors": ["yellow", "gray", "green"],
        "size": ["M", "L", "XL"],
        "tagsname": ["Debit Card"],
        "rating": 4,
        "variants": [
        {
          "color": "yellow",
          "images": "/assets/images/fashion/product/1.jpg"
        },
        {
          "color": "gray",
          "images": "/assets/images/fashion/product/21.jpg"
        },
        {
          "color": "green",
          "images": "/assets/images/fashion/product/36.jpg"
        }]
      },
      {
        "id": 81,
        "selector":"selector",
        "name": "Flare Dress",
        "price": 120,
        "salePrice": 200,
        "discount": 50,
        "pictures": ["/assets/images/fashion/product/1.jpg", "/assets/images/fashion/product/21.jpg", "/assets/images/fashion/product/36.jpg", "/assets/images/fashion/product/12.jpg"],
        "shortDetails": "Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. Nemo enim ipsam voluptatem,",
        "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        "stock": 16,
        "new": true,
        "sale": true,
        "category": "women",
        "colors": ["yellow", "gray", "green"],
        "size": ["M", "L", "XL"],
        "tagsname": ["Exchange"],
        "rating": 4,
        "variants": [
        {
          "color": "yellow",
          "images": "/assets/images/fashion/product/1.jpg"
        },
        {
          "color": "gray",
          "images": "/assets/images/fashion/product/21.jpg"
        },
        {
          "color": "green",
          "images": "/assets/images/fashion/product/36.jpg"
        }]
      },
      {
        "id": 82,
        "selector":"selector",
        "name": "Flare Dress",
        "price": 120,
        "salePrice": 200,
        "discount": 50,
        "pictures": ["/assets/images/fashion/product/1.jpg", "/assets/images/fashion/product/21.jpg", "/assets/images/fashion/product/36.jpg", "/assets/images/fashion/product/12.jpg"],
        "shortDetails": "Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. Nemo enim ipsam voluptatem,",
        "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        "stock": 16,
        "new": true,
        "sale": true,
        "category": "women",
        "colors": ["yellow", "gray", "green"],
        "size": ["M", "L", "XL"],
        "tagsname": ["Identity"],
        "rating": 4,
        "variants": [
        {
          "color": "yellow",
          "images": "/assets/images/fashion/product/1.jpg"
        },
        {
          "color": "gray",
          "images": "/assets/images/fashion/product/21.jpg"
        },
        {
          "color": "green",
          "images": "/assets/images/fashion/product/36.jpg"
        }]
      },
      {
        "id": 83,
        "selector":"selector",
        "name": "Flare Dress",
        "price": 120,
        "salePrice": 200,
        "discount": 50,
        "pictures": ["/assets/images/fashion/product/1.jpg", "/assets/images/fashion/product/21.jpg", "/assets/images/fashion/product/36.jpg", "/assets/images/fashion/product/12.jpg"],
        "shortDetails": "Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. Nemo enim ipsam voluptatem,",
        "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        "stock": 16,
        "new": true,
        "sale": true,
        "category": "women",
        "colors": ["yellow", "gray", "green"],
        "size": ["M", "L", "XL"],
        "tagsname": ["Marketplace"],
        "rating": 4,
        "variants": [
        {
          "color": "yellow",
          "images": "/assets/images/fashion/product/1.jpg"
        },
        {
          "color": "gray",
          "images": "/assets/images/fashion/product/21.jpg"
        },
        {
          "color": "green",
          "images": "/assets/images/fashion/product/36.jpg"
        }]
      },
      {
        "id": 84,
        "selector":"selector",
        "name": "Flare Dress",
        "price": 120,
        "salePrice": 200,
        "discount": 50,
        "pictures": ["/assets/images/fashion/product/1.jpg", "/assets/images/fashion/product/21.jpg", "/assets/images/fashion/product/36.jpg", "/assets/images/fashion/product/12.jpg"],
        "shortDetails": "Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. Nemo enim ipsam voluptatem,",
        "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        "stock": 16,
        "new": true,
        "sale": true,
        "category": "women",
        "colors": ["yellow", "gray", "green"],
        "size": ["M", "L", "XL"],
        "tagsname": ["Real Estate"],
        "rating": 4,
        "variants": [
        {
          "color": "yellow",
          "images": "/assets/images/fashion/product/1.jpg"
        },
        {
          "color": "gray",
          "images": "/assets/images/fashion/product/21.jpg"
        },
        {
          "color": "green",
          "images": "/assets/images/fashion/product/36.jpg"
        }]
      },
      {
        "id": 85,
        "selector":"selector",
        "name": "Flare Dress",
        "price": 120,
        "salePrice": 200,
        "discount": 50,
        "pictures": ["/assets/images/fashion/product/1.jpg", "/assets/images/fashion/product/21.jpg", "/assets/images/fashion/product/36.jpg", "/assets/images/fashion/product/12.jpg"],
        "shortDetails": "Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. Nemo enim ipsam voluptatem,",
        "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        "stock": 16,
        "new": true,
        "sale": true,
        "category": "women",
        "colors": ["yellow", "gray", "green"],
        "size": ["M", "L", "XL"],
        "tagsname": ["Transportation"],
        "rating": 4,
        "variants": [
        {
          "color": "yellow",
          "images": "/assets/images/fashion/product/1.jpg"
        },
        {
          "color": "gray",
          "images": "/assets/images/fashion/product/21.jpg"
        },
        {
          "color": "green",
          "images": "/assets/images/fashion/product/36.jpg"
        }]
      },
      {
        "id": 86,
        "selector":"selector",
        "name": "Flare Dress",
        "price": 120,
        "salePrice": 200,
        "discount": 50,
        "pictures": ["/assets/images/fashion/product/1.jpg", "/assets/images/fashion/product/21.jpg", "/assets/images/fashion/product/36.jpg", "/assets/images/fashion/product/12.jpg"],
        "shortDetails": "Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. Nemo enim ipsam voluptatem,",
        "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        "stock": 16,
        "new": true,
        "sale": true,
        "category": "women",
        "colors": ["yellow", "gray", "green"],
        "size": ["M", "L", "XL"],
        "tagsname": ["Agriculture"],
        "rating": 4,
        "variants": [
        {
          "color": "yellow",
          "images": "/assets/images/fashion/product/1.jpg"
        },
        {
          "color": "gray",
          "images": "/assets/images/fashion/product/21.jpg"
        },
        {
          "color": "green",
          "images": "/assets/images/fashion/product/36.jpg"
        }]
      },
      {
        "id": 87,
        "selector":"selector",
        "name": "Flare Dress",
        "price": 120,
        "salePrice": 200,
        "discount": 50,
        "pictures": ["/assets/images/fashion/product/1.jpg", "/assets/images/fashion/product/21.jpg", "/assets/images/fashion/product/36.jpg", "/assets/images/fashion/product/12.jpg"],
        "shortDetails": "Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. Nemo enim ipsam voluptatem,",
        "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        "stock": 16,
        "new": true,
        "sale": true,
        "category": "women",
        "colors": ["yellow", "gray", "green"],
        "size": ["M", "L", "XL"],
        "tagsname": ["Charity"],
        "rating": 4,
        "variants": [
        {
          "color": "yellow",
          "images": "/assets/images/fashion/product/1.jpg"
        },
        {
          "color": "gray",
          "images": "/assets/images/fashion/product/21.jpg"
        },
        {
          "color": "green",
          "images": "/assets/images/fashion/product/36.jpg"
        }]
      },
      {
        "id": 88,
        "selector":"selector",
        "name": "Flare Dress",
        "price": 120,
        "salePrice": 200,
        "discount": 50,
        "pictures": ["/assets/images/fashion/product/1.jpg", "/assets/images/fashion/product/21.jpg", "/assets/images/fashion/product/36.jpg", "/assets/images/fashion/product/12.jpg"],
        "shortDetails": "Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. Nemo enim ipsam voluptatem,",
        "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        "stock": 16,
        "new": true,
        "sale": true,
        "category": "women",
        "colors": ["yellow", "gray", "green"],
        "size": ["M", "L", "XL"],
        "tagsname": ["eCommerce"],
        "rating": 4,
        "variants": [
        {
          "color": "yellow",
          "images": "/assets/images/fashion/product/1.jpg"
        },
        {
          "color": "gray",
          "images": "/assets/images/fashion/product/21.jpg"
        },
        {
          "color": "green",
          "images": "/assets/images/fashion/product/36.jpg"
        }]
      },
      {
        "id": 89,
        "selector":"selector",
        "name": "Flare Dress",
        "price": 120,
        "salePrice": 200,
        "discount": 50,
        "pictures": ["/assets/images/fashion/product/1.jpg", "/assets/images/fashion/product/21.jpg", "/assets/images/fashion/product/36.jpg", "/assets/images/fashion/product/12.jpg"],
        "shortDetails": "Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. Nemo enim ipsam voluptatem,",
        "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        "stock": 16,
        "new": true,
        "sale": true,
        "category": "women",
        "colors": ["yellow", "gray", "green"],
        "size": ["M", "L", "XL"],
        "tagsname": ["Financial Service"],
        "rating": 4,
        "variants": [
        {
          "color": "yellow",
          "images": "/assets/images/fashion/product/1.jpg"
        },
        {
          "color": "gray",
          "images": "/assets/images/fashion/product/21.jpg"
        },
        {
          "color": "green",
          "images": "/assets/images/fashion/product/36.jpg"
        }]
      },
      {
        "id": 90,
        "selector":"selector",
        "name": "Flare Dress",
        "price": 120,
        "salePrice": 200,
        "discount": 50,
        "pictures": ["/assets/images/fashion/product/1.jpg", "/assets/images/fashion/product/21.jpg", "/assets/images/fashion/product/36.jpg", "/assets/images/fashion/product/12.jpg"],
        "shortDetails": "Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. Nemo enim ipsam voluptatem,",
        "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        "stock": 16,
        "new": true,
        "sale": true,
        "category": "women",
        "colors": ["yellow", "gray", "green"],
        "size": ["M", "L", "XL"],
        "tagsname": ["Infrastructure"],
        "rating": 4,
        "variants": [
        {
          "color": "yellow",
          "images": "/assets/images/fashion/product/1.jpg"
        },
        {
          "color": "gray",
          "images": "/assets/images/fashion/product/21.jpg"
        },
        {
          "color": "green",
          "images": "/assets/images/fashion/product/36.jpg"
        }]
      },
      {
        "id": 91,
        "selector":"selector",
        "name": "Flare Dress",
        "price": 120,
        "salePrice": 200,
        "discount": 50,
        "pictures": ["/assets/images/fashion/product/1.jpg", "/assets/images/fashion/product/21.jpg", "/assets/images/fashion/product/36.jpg", "/assets/images/fashion/product/12.jpg"],
        "shortDetails": "Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. Nemo enim ipsam voluptatem,",
        "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        "stock": 16,
        "new": true,
        "sale": true,
        "category": "women",
        "colors": ["yellow", "gray", "green"],
        "size": ["M", "L", "XL"],
        "tagsname": ["Media"],
        "rating": 4,
        "variants": [
        {
          "color": "yellow",
          "images": "/assets/images/fashion/product/1.jpg"
        },
        {
          "color": "gray",
          "images": "/assets/images/fashion/product/21.jpg"
        },
        {
          "color": "green",
          "images": "/assets/images/fashion/product/36.jpg"
        }]
      },
      {
        "id": 92,
        "selector":"selector",
        "name": "Flare Dress",
        "price": 120,
        "salePrice": 200,
        "discount": 50,
        "pictures": ["/assets/images/fashion/product/1.jpg", "/assets/images/fashion/product/21.jpg", "/assets/images/fashion/product/36.jpg", "/assets/images/fashion/product/12.jpg"],
        "shortDetails": "Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. Nemo enim ipsam voluptatem,",
        "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        "stock": 16,
        "new": true,
        "sale": true,
        "category": "women",
        "colors": ["yellow", "gray", "green"],
        "size": ["M", "L", "XL"],
        "tagsname": ["Social"],
        "rating": 4,
        "variants": [
        {
          "color": "yellow",
          "images": "/assets/images/fashion/product/1.jpg"
        },
        {
          "color": "gray",
          "images": "/assets/images/fashion/product/21.jpg"
        },
        {
          "color": "green",
          "images": "/assets/images/fashion/product/36.jpg"
        }]
      },
      {
        "id": 93,
        "selector":"selector",
        "name": "Flare Dress",
        "price": 120,
        "salePrice": 200,
        "discount": 50,
        "pictures": ["/assets/images/fashion/product/1.jpg", "/assets/images/fashion/product/21.jpg", "/assets/images/fashion/product/36.jpg", "/assets/images/fashion/product/12.jpg"],
        "shortDetails": "Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. Nemo enim ipsam voluptatem,",
        "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        "stock": 16,
        "new": true,
        "sale": true,
        "category": "women",
        "colors": ["yellow", "gray", "green"],
        "size": ["M", "L", "XL"],
        "tagsname": ["Travel"],
        "rating": 4,
        "variants": [
        {
          "color": "yellow",
          "images": "/assets/images/fashion/product/1.jpg"
        },
        {
          "color": "gray",
          "images": "/assets/images/fashion/product/21.jpg"
        },
        {
          "color": "green",
          "images": "/assets/images/fashion/product/36.jpg"
        }]
      },
      {
        "id": 94,
        "selector":"selector",
        "name": "Flare Dress",
        "price": 120,
        "salePrice": 200,
        "discount": 50,
        "pictures": ["/assets/images/fashion/product/1.jpg", "/assets/images/fashion/product/21.jpg", "/assets/images/fashion/product/36.jpg", "/assets/images/fashion/product/12.jpg"],
        "shortDetails": "Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. Nemo enim ipsam voluptatem,",
        "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        "stock": 16,
        "new": true,
        "sale": true,
        "category": "women",
        "colors": ["yellow", "gray", "green"],
        "size": ["M", "L", "XL"],
        "tagsname": ["AI"],
        "rating": 4,
        "variants": [
        {
          "color": "yellow",
          "images": "/assets/images/fashion/product/1.jpg"
        },
        {
          "color": "gray",
          "images": "/assets/images/fashion/product/21.jpg"
        },
        {
          "color": "green",
          "images": "/assets/images/fashion/product/36.jpg"
        }]
      },
      {
        "id": 95,
        "selector":"selector",
        "name": "Flare Dress",
        "price": 120,
        "salePrice": 200,
        "discount": 50,
        "pictures": ["/assets/images/fashion/product/1.jpg", "/assets/images/fashion/product/21.jpg", "/assets/images/fashion/product/36.jpg", "/assets/images/fashion/product/12.jpg"],
        "shortDetails": "Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. Nemo enim ipsam voluptatem,",
        "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        "stock": 16,
        "new": true,
        "sale": true,
        "category": "women",
        "colors": ["yellow", "gray", "green"],
        "size": ["M", "L", "XL"],
        "tagsname": ["Communication"],
        "rating": 4,
        "variants": [
        {
          "color": "yellow",
          "images": "/assets/images/fashion/product/1.jpg"
        },
        {
          "color": "gray",
          "images": "/assets/images/fashion/product/21.jpg"
        },
        {
          "color": "green",
          "images": "/assets/images/fashion/product/36.jpg"
        }]
      },
      {
        "id": 96,
        "selector":"selector",
        "name": "Flare Dress",
        "price": 120,
        "salePrice": 200,
        "discount": 50,
        "pictures": ["/assets/images/fashion/product/1.jpg", "/assets/images/fashion/product/21.jpg", "/assets/images/fashion/product/36.jpg", "/assets/images/fashion/product/12.jpg"],
        "shortDetails": "Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. Nemo enim ipsam voluptatem,",
        "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        "stock": 16,
        "new": true,
        "sale": true,
        "category": "women",
        "colors": ["yellow", "gray", "green"],
        "size": ["M", "L", "XL"],
        "tagsname": ["Education"],
        "rating": 4,
        "variants": [
        {
          "color": "yellow",
          "images": "/assets/images/fashion/product/1.jpg"
        },
        {
          "color": "gray",
          "images": "/assets/images/fashion/product/21.jpg"
        },
        {
          "color": "green",
          "images": "/assets/images/fashion/product/36.jpg"
        }]
      },
      {
        "id": 97,
        "selector":"selector",
        "name": "Flare Dress",
        "price": 120,
        "salePrice": 200,
        "discount": 50,
        "pictures": ["/assets/images/fashion/product/1.jpg", "/assets/images/fashion/product/21.jpg", "/assets/images/fashion/product/36.jpg", "/assets/images/fashion/product/12.jpg"],
        "shortDetails": "Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. Nemo enim ipsam voluptatem,",
        "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        "stock": 16,
        "new": true,
        "sale": true,
        "category": "women",
        "colors": ["yellow", "gray", "green"],
        "size": ["M", "L", "XL"],
        "tagsname": ["Gambling"],
        "rating": 4,
        "variants": [
        {
          "color": "yellow",
          "images": "/assets/images/fashion/product/1.jpg"
        },
        {
          "color": "gray",
          "images": "/assets/images/fashion/product/21.jpg"
        },
        {
          "color": "green",
          "images": "/assets/images/fashion/product/36.jpg"
        }]
      },
      {
        "id": 98,
        "selector":"selector",
        "name": "Flare Dress",
        "price": 120,
        "salePrice": 200,
        "discount": 50,
        "pictures": ["/assets/images/fashion/product/1.jpg", "/assets/images/fashion/product/21.jpg", "/assets/images/fashion/product/36.jpg", "/assets/images/fashion/product/12.jpg"],
        "shortDetails": "Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. Nemo enim ipsam voluptatem,",
        "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        "stock": 16,
        "new": true,
        "sale": true,
        "category": "women",
        "colors": ["yellow", "gray", "green"],
        "size": ["M", "L", "XL"],
        "tagsname": ["Investing Tool"],
        "rating": 4,
        "variants": [
        {
          "color": "yellow",
          "images": "/assets/images/fashion/product/1.jpg"
        },
        {
          "color": "gray",
          "images": "/assets/images/fashion/product/21.jpg"
        },
        {
          "color": "green",
          "images": "/assets/images/fashion/product/36.jpg"
        }]
      },
      {
        "id": 99,
        "selector":"selector",
        "name": "Flare Dress",
        "price": 120,
        "salePrice": 200,
        "discount": 50,
        "pictures": ["/assets/images/fashion/product/1.jpg", "/assets/images/fashion/product/21.jpg", "/assets/images/fashion/product/36.jpg", "/assets/images/fashion/product/12.jpg"],
        "shortDetails": "Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. Nemo enim ipsam voluptatem,",
        "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        "stock": 16,
        "new": true,
        "sale": true,
        "category": "women",
        "colors": ["yellow", "gray", "green"],
        "size": ["M", "L", "XL"],
        "tagsname": ["Meme"],
        "rating": 4,
        "variants": [
        {
          "color": "yellow",
          "images": "/assets/images/fashion/product/1.jpg"
        },
        {
          "color": "gray",
          "images": "/assets/images/fashion/product/21.jpg"
        },
        {
          "color": "green",
          "images": "/assets/images/fashion/product/36.jpg"
        }]
      },
      {
        "id": 100,
        "selector":"selector",
        "name": "Flare Dress",
        "price": 120,
        "salePrice": 200,
        "discount": 50,
        "pictures": ["/assets/images/fashion/product/1.jpg", "/assets/images/fashion/product/21.jpg", "/assets/images/fashion/product/36.jpg", "/assets/images/fashion/product/12.jpg"],
        "shortDetails": "Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. Nemo enim ipsam voluptatem,",
        "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        "stock": 16,
        "new": true,
        "sale": true,
        "category": "women",
        "colors": ["yellow", "gray", "green"],
        "size": ["M", "L", "XL"],
        "tagsname": ["Software"],
        "rating": 4,
        "variants": [
        {
          "color": "yellow",
          "images": "/assets/images/fashion/product/1.jpg"
        },
        {
          "color": "gray",
          "images": "/assets/images/fashion/product/21.jpg"
        },
        {
          "color": "green",
          "images": "/assets/images/fashion/product/36.jpg"
        }]
      },
      {
        "id": 101,
        "selector":"selector",
        "name": "Flare Dress",
        "price": 120,
        "salePrice": 200,
        "discount": 50,
        "pictures": ["/assets/images/fashion/product/1.jpg", "/assets/images/fashion/product/21.jpg", "/assets/images/fashion/product/36.jpg", "/assets/images/fashion/product/12.jpg"],
        "shortDetails": "Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. Nemo enim ipsam voluptatem,",
        "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        "stock": 16,
        "new": true,
        "sale": true,
        "category": "women",
        "colors": ["yellow", "gray", "green"],
        "size": ["M", "L", "XL"],
        "tagsname": ["Virtual Reality"],
        "rating": 4,
        "variants": [
        {
          "color": "yellow",
          "images": "/assets/images/fashion/product/1.jpg"
        },
        {
          "color": "gray",
          "images": "/assets/images/fashion/product/21.jpg"
        },
        {
          "color": "green",
          "images": "/assets/images/fashion/product/36.jpg"
        }]
      },
      {
        "id": 102,
        "selector":"selector",
        "name": "Flare Dress",
        "price": 120,
        "salePrice": 200,
        "discount": 50,
        "pictures": ["/assets/images/fashion/product/1.jpg", "/assets/images/fashion/product/21.jpg", "/assets/images/fashion/product/36.jpg", "/assets/images/fashion/product/12.jpg"],
        "shortDetails": "Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. Nemo enim ipsam voluptatem,",
        "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        "stock": 16,
        "new": true,
        "sale": true,
        "category": "women",
        "colors": ["yellow", "gray", "green"],
        "size": ["M", "L", "XL"],
        "tagsname": ["Art"],
        "rating": 4,
        "variants": [
        {
          "color": "yellow",
          "images": "/assets/images/fashion/product/1.jpg"
        },
        {
          "color": "gray",
          "images": "/assets/images/fashion/product/21.jpg"
        },
        {
          "color": "green",
          "images": "/assets/images/fashion/product/36.jpg"
        }]
      },
      {
        "id": 103,
        "selector":"selector",
        "name": "Flare Dress",
        "price": 120,
        "salePrice": 200,
        "discount": 50,
        "pictures": ["/assets/images/fashion/product/1.jpg", "/assets/images/fashion/product/21.jpg", "/assets/images/fashion/product/36.jpg", "/assets/images/fashion/product/12.jpg"],
        "shortDetails": "Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. Nemo enim ipsam voluptatem,",
        "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        "stock": 16,
        "new": true,
        "sale": true,
        "category": "women",
        "colors": ["yellow", "gray", "green"],
        "size": ["M", "L", "XL"],
        "tagsname": ["Computing"],
        "rating": 4,
        "variants": [
        {
          "color": "yellow",
          "images": "/assets/images/fashion/product/1.jpg"
        },
        {
          "color": "gray",
          "images": "/assets/images/fashion/product/21.jpg"
        },
        {
          "color": "green",
          "images": "/assets/images/fashion/product/36.jpg"
        }]
      },
      {
        "id": 104,
        "selector":"selector",
        "name": "Flare Dress",
        "price": 120,
        "salePrice": 200,
        "discount": 50,
        "pictures": ["/assets/images/fashion/product/1.jpg", "/assets/images/fashion/product/21.jpg", "/assets/images/fashion/product/36.jpg", "/assets/images/fashion/product/12.jpg"],
        "shortDetails": "Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. Nemo enim ipsam voluptatem,",
        "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        "stock": 16,
        "new": true,
        "sale": true,
        "category": "women",
        "colors": ["yellow", "gray", "green"],
        "size": ["M", "L", "XL"],
        "tagsname": ["Energy"],
        "rating": 4,
        "variants": [
        {
          "color": "yellow",
          "images": "/assets/images/fashion/product/1.jpg"
        },
        {
          "color": "gray",
          "images": "/assets/images/fashion/product/21.jpg"
        },
        {
          "color": "green",
          "images": "/assets/images/fashion/product/36.jpg"
        }]
      },
      {
        "id": 105,
        "selector":"selector",
        "name": "Flare Dress",
        "price": 120,
        "salePrice": 200,
        "discount": 50,
        "pictures": ["/assets/images/fashion/product/1.jpg", "/assets/images/fashion/product/21.jpg", "/assets/images/fashion/product/36.jpg", "/assets/images/fashion/product/12.jpg"],
        "shortDetails": "Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. Nemo enim ipsam voluptatem,",
        "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        "stock": 16,
        "new": true,
        "sale": true,
        "category": "women",
        "colors": ["yellow", "gray", "green"],
        "size": ["M", "L", "XL"],
        "tagsname": ["Gaming"],
        "rating": 4,
        "variants": [
        {
          "color": "yellow",
          "images": "/assets/images/fashion/product/1.jpg"
        },
        {
          "color": "gray",
          "images": "/assets/images/fashion/product/21.jpg"
        },
        {
          "color": "green",
          "images": "/assets/images/fashion/product/36.jpg"
        }]
      },
      {
        "id": 106,
        "selector":"selector",
        "name": "Flare Dress",
        "price": 120,
        "salePrice": 200,
        "discount": 50,
        "pictures": ["/assets/images/fashion/product/1.jpg", "/assets/images/fashion/product/21.jpg", "/assets/images/fashion/product/36.jpg", "/assets/images/fashion/product/12.jpg"],
        "shortDetails": "Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. Nemo enim ipsam voluptatem,",
        "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        "stock": 16,
        "new": true,
        "sale": true,
        "category": "women",
        "colors": ["yellow", "gray", "green"],
        "size": ["M", "L", "XL"],
        "tagsname": ["IoT"],
        "rating": 4,
        "variants": [
        {
          "color": "yellow",
          "images": "/assets/images/fashion/product/1.jpg"
        },
        {
          "color": "gray",
          "images": "/assets/images/fashion/product/21.jpg"
        },
        {
          "color": "green",
          "images": "/assets/images/fashion/product/36.jpg"
        }]
      },
      {
        "id": 107,
        "selector":"selector",
        "name": "Flare Dress",
        "price": 120,
        "salePrice": 200,
        "discount": 50,
        "pictures": ["/assets/images/fashion/product/1.jpg", "/assets/images/fashion/product/21.jpg", "/assets/images/fashion/product/36.jpg", "/assets/images/fashion/product/12.jpg"],
        "shortDetails": "Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. Nemo enim ipsam voluptatem,",
        "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        "stock": 16,
        "new": true,
        "sale": true,
        "category": "women",
        "colors": ["yellow", "gray", "green"],
        "size": ["M", "L", "XL"],
        "tagsname": ["Mining Facility"],
        "rating": 4,
        "variants": [
        {
          "color": "yellow",
          "images": "/assets/images/fashion/product/1.jpg"
        },
        {
          "color": "gray",
          "images": "/assets/images/fashion/product/21.jpg"
        },
        {
          "color": "green",
          "images": "/assets/images/fashion/product/36.jpg"
        }]
      },
      {
        "id": 108,
        "selector":"selector",
        "name": "Flare Dress",
        "price": 120,
        "salePrice": 200,
        "discount": 50,
        "pictures": ["/assets/images/fashion/product/1.jpg", "/assets/images/fashion/product/21.jpg", "/assets/images/fashion/product/36.jpg", "/assets/images/fashion/product/12.jpg"],
        "shortDetails": "Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. Nemo enim ipsam voluptatem,",
        "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        "stock": 16,
        "new": true,
        "sale": true,
        "category": "women",
        "colors": ["yellow", "gray", "green"],
        "size": ["M", "L", "XL"],
        "tagsname": ["Storage"],
        "rating": 4,
        "variants": [
        {
          "color": "yellow",
          "images": "/assets/images/fashion/product/1.jpg"
        },
        {
          "color": "gray",
          "images": "/assets/images/fashion/product/21.jpg"
        },
        {
          "color": "green",
          "images": "/assets/images/fashion/product/36.jpg"
        }]
      },
      {
        "id": 109,
        "selector":"selector",
        "name": "Flare Dress",
        "price": 120,
        "salePrice": 200,
        "discount": 50,
        "pictures": ["/assets/images/fashion/product/1.jpg", "/assets/images/fashion/product/21.jpg", "/assets/images/fashion/product/36.jpg", "/assets/images/fashion/product/12.jpg"],
        "shortDetails": "Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. Nemo enim ipsam voluptatem,",
        "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        "stock": 16,
        "new": true,
        "sale": true,
        "category": "women",
        "colors": ["yellow", "gray", "green"],
        "size": ["M", "L", "XL"],
        "tagsname": ["Adandoned"],
        "rating": 4,
        "variants": [
        {
          "color": "yellow",
          "images": "/assets/images/fashion/product/1.jpg"
        },
        {
          "color": "gray",
          "images": "/assets/images/fashion/product/21.jpg"
        },
        {
          "color": "green",
          "images": "/assets/images/fashion/product/36.jpg"
        }]
      },
      
      ]

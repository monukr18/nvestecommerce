import React, { Component } from 'react';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import {connect} from 'react-redux'

import {getBestSeller,BestSellerProducts, getBestSellerProducts, getMensWear, getNewProducts, getWomensWear} from '../../../services/index'
import {addToCart, addToWishlist, addToCompare} from "../../../actions/index";
import ProductItem from './product-item';

class SpecialProducts extends Component {
    render (){

        const {BestSellerProducts,bestSeller,newProducts, featuredProducts, symbol, addToCart, addToWishlist, addToCompare} = this.props

        return (
                <section className="section-b-space ratio_square">
                    <div className="container-fluid">
                        <div className="title2">
                            <h4>The Latest & Greatest</h4>
                            <h2 className="title-inner2">Tokenized Offerings</h2>
                        </div>
                        <div className="row">
                            <div className="col">
                                <div className="theme-tab layout7-product">
                                    <Tabs className="theme-tab">
                                        <TabList  className="tabs tab-title">
                                            <Tab>Exchange Offerings</Tab>
                                            <Tab>Fund Offerings </Tab>
                                            <Tab>Index Offerings</Tab>
                                        </TabList>

                                        <TabPanel>
                                            <div className="no-slider row">
                                                { newProducts.map((product, index ) =>
                                                    <ProductItem product={product} symbol={symbol}
                                                                 onAddToCompareClicked={() => addToCompare(product)}
                                                                 onAddToWishlistClicked={() => addToWishlist(product)}
                                                                 onAddToCartClicked={() => addToCart(product, 1)} key={index} /> )
                                                }
                                            </div>
                                        </TabPanel>
                                        <TabPanel>
                                            <div className="no-slider row">
                                                { featuredProducts.map((product, index ) =>
                                                    <ProductItem product={product} symbol={symbol}
                                                                 onAddToCompareClicked={() => addToCompare(product)}
                                                                 onAddToWishlistClicked={() => addToWishlist(product)}
                                                                 onAddToCartClicked={() => addToCart(product, 1)} key={index} /> )
                                                }
                                            </div>
                                        </TabPanel>
                                        <TabPanel>
                                            <div className=" no-slider row">
                                                { bestSeller.map((product, index ) =>
                                                    <ProductItem product={product} symbol={symbol}
                                                                 onAddToCompareClicked={() => addToCompare(product)}
                                                                 onAddToWishlistClicked={() => addToWishlist(product)}
                                                                 onAddToCartClicked={() => addToCart(product, 1)} key={index} /> )
                                                }
                                            </div>
                                        </TabPanel>
                                    </Tabs>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
        )
    }
}

const mapStateToProps = (state, ownProps) => ({
    bestSeller: getBestSellerProducts(state.data.products, ownProps.type),
    newProducts: getNewProducts(state.data.products, ownProps.type),
    featuredProducts: BestSellerProducts(state.data.products, ownProps.type),
    symbol: state.data.symbol
})

export default connect(mapStateToProps, {addToCart, addToWishlist, addToCompare}) (SpecialProducts);